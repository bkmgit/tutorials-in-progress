{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Running a (short) ensemble of Alanine Dipeptide simulations\n",
    "To do serious Markov State Modeling on realistic systems you will be using quite a bit of computational resources - more than what is realistic to achieve in a 2h lab during a workshop, in particular when there are also 30-50 participants competing for nodes.\n",
    "\n",
    "However, to at least give you a taste of the concept we will use a close-to-trivial system: The Alanine Dipeptide. In practice we will be simulating more water than protein, but this is also a good challenge in terms of learning how to push simulations by altering settings and running multiple simulations per node for better throughput. Anything larger is going to scale much better.\n",
    "\n",
    "Before starting the actual Markov State Modeling, you will\n",
    "* Prepare and relax up to 16 different simulations with virtual interaction sites in a rhombic dodecahedron box\n",
    "* Optional: Play around with the number of OpenMP threads to decide how many simulations to run on your node.\n",
    "* Run the simulations with the -multidir option to gmx mdrun\n",
    "* Extract just the protein part of the trajectory (to avoid confusing the Markov State Model with the vsite mass centers)\n",
    "\n",
    "For a normal (large) system you should put serious thought into how to come up with reasonable starting states, but in this case we have simply generated 16 different conformations by simulating the system at high temperature (500K). The tarball will unpack into a handful of parameters files and 16 subdirectories - one per starting conformation. \n",
    "\n",
    "These systems are actually so small that we will spend most of the time on integration/constraints, but since it's a proof-of-principle we will still set this up the way we would try to optimise a larger system for performance. First we use the pdb2gmx command to generate topologies (including vsites) with the amber99sb-ildn force field and tip3p water for each starting conformation. Since we're a bit lazy, we might as well use a bash script instead of going into each directory:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook is currently running on one of Tegner's login or transfer nodes. This is a good place to run very light-weight programs, but simulations unfortunately does not belong to that category. Therefore, leave the notebook running on Tegner and open a new terminal window and login to Beskow. You can then copy the commands from this notebook and run in your other terminal. You can also run this on your local machine."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "pdc-ssh <username>@beskow.pdc.kth.se"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since alanine dipeptide is so small our preparation fo the files will be very fast, therefore we are going to run the following commands on Beskow's login node. If you have bigger systems you should make any pre-processing on Tegner (or your local computer)! For practical reasons this is not possible in this course.\n",
    "\n",
    "First, we need to load the module we are going to need."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "module load gromacs/2019.2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for i in {0..15}; do cd run${i} ; gmx pdb2gmx -f start.pdb -ff amber99sb-ildn -water tip3p -vsite hydro ; cd .. ; done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a second step, we now edit the conformation of each system to make the box a rhombic dodecahedron, with distance 1.0 nm between the protein and the box edge, and add water. Roughly how many atoms do you end up with per system?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for i in {0..15}; do gmx editconf -bt dodeca -d 1.0 -f run${i}/conf.gro -o run${i}/box.gro ; done\n",
    "for i in {0..15}; do gmx solvate -cs spc216.gro -cp run${i}/box.gro -o run${i}/solvated.gro -p run${i}/topol.top ; done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unfortuantely there is ONE shortcoming in this force field: It does not include a couple of parameters we need to apply the constraints for the ACE/NME termini in the dipeptide. That's no big problem - grompp will warn you about it and refuse to produce input. There are several possibilities to solve this: (1) we could copy the default force field to our own directory and add the parameters, or (2) we can add the parameters to the top of each topol.top file. We could also do things like add the parameters explicitly on the line they are used, or have all topol.top files include a common external file with the protein definition, but let's not make this too complicated. Add these lines just below the #include line in the beginning of each topol.top that includes the force field parameters (unless you are a sed wizard, this is likely easiest to do manually by editing each file) so the file looks like this:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "; Include forcefield parameters\n",
    "#include \"amber99sb-ildn.ff/forcefield.itp\"\n",
    "\n",
    "[ constrainttypes ]\n",
    "; constraints for capped termini\n",
    "MCH3 C 2 0.166426\n",
    "MCH3 C 2 0.166426\n",
    "N MCH3 2 0.166426\n",
    "N MCH3 2 0.166426"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you should be able to preprocess the data and run the energy minimization (just 100 steps):"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since our energy minimization only will take a few seconds we are going to run this on the login node. When energy minimizing bigger systems make sure to run on a compute node!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for i in {0..15}; do gmx grompp -f em -p run${i}/topol.top  -c run${i}/solvated.gro -o run${i}/em.tpr ; done\n",
    "for i in {0..15}; do cd run${i} ; gmx mdrun -s em.tpr ; cd .. ; done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now ready to prepare the actual run, which is the last step we'll do on the login node or local machine:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for i in {0..15}; do gmx grompp -f run -p run${i}/topol.top -c run${i}/confout.gro -o run${i}/run.tpr ; done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point, you now have 16 prepared simulations, each 10ns in length. However, since the system is tiny there is no way you are going to scale it efficienctly on one node - and that would anyway be stupid since you have 16 of them you need to push through. Instead, prepare a batch script where you use the -multidir option to mdrun. Since the nodes you will be using have 32 cores / 64 threads, a good starting point might be to pick 16 MPI ranks that each use 4 OpenMP threads - then all the simulations should finish in about 45 minutes. If you are uncertain, it's a VERY good idea to test performance with a shorter simulation, and/or check the output as the simulations are running to make sure nothing is wrong."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, use and edit the MPI ranks/OpenMP threads in the provided batch script, called run.sh and submit each simulation as a job."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for i in {0..15}; do sbatch run.sh; done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the simulations are done you have a LOT of data since we saved output every 200 steps. That is ridiculously dense for any normal MSM, but since the dynamics of the alanine dipeptide is so fast we need it to at least illustrate the difference between non-markovian and markovian time scales for you here!\n",
    "\n",
    "Most of the MSM tools (including PyEMMA) can read GROMACS files, but there are two challenges:\n",
    "\n",
    "1. It will be exceptionally inefficient to read all the water in each frame, just to discard it\n",
    "2. The mass center particules used for the virtual sites will confuse some features of PyEMMA\n",
    "\n",
    "Instead of waiting forever (or running out of memory), it's easier to address this by extracting a coordinate file and trajectories containing just the protein. We'll do this in two steps:\n",
    "\n",
    "1. Create an index file that selects just the actual protein atoms\n",
    "2. Use this index file with gmx trjconv to extract trajectories (and a reference structure) with just the protein"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "gmx make_ndx -f run0/confout.gro \n",
    "# Type \"1 & !a M*\" (without the quotation marks) to select all particules that are part of the protein group\n",
    "# but that do not have atom names starting with \"M\".\n",
    "# As a second step, you might want to name this group (e.g. #11) something reasonable (actualprotein?) with\n",
    "# the command \"name 11 actualprotein\". Then type \"q\" to save the new index file.\n",
    "\n",
    "# Create a new folder for the MSM analysis\n",
    "mkdir msm\n",
    "# Export the reference structure and each trajectory - pick your group when asked!\n",
    "gmx trjconv -s run0/confout.gro -n index -o msm/reference.pdb\n",
    "for i in {0..15}; do gmx trjconv -s run${i}/run.tpr -f run${i}/traj_comp.xtc -n index -o msm/traj${i}.xtc ; done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point we are done with the simulations! You should now have a reference.pdb file and 16 trajectories in the msm directory, and we are ready to start the actual Markov State Modeling part. For this demo we will only do one step, but if this was a gigantic ion channel, the idea is that we should now use the MSM after the first 'generation' of simulations to decide what states we should start the second generation of simulations from."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Markov State Modeling of Alanine Dipeptide\n",
    "Now that you have generated some simulations of alanine dipeptide, it is time to analyze the data! From now on you can use the Jupyter notebook as usual and execute the cells on Tegner's login node. Markov state models (MSMs) are very powerful as they can give you much information about how your system behaves in a statistically robust manner as it gives us an ensemble view of the dynamics. You \"just\" need to sample the equilibrium distribution reasonably well.\n",
    "\n",
    "The MSM is actually a transition probability matrix which can be obtained by counting the transitions between states (more on that later) that are observed in your simulations. The eigenvalues and eigenvectors of this matrix represent some nice physical properties that we may be interested in. The first eigenvector always represents the equilibrium distribution, while the following eigenvectors each represents a process that decays to this equilibrium. The corresponding eigenvalue is related to the timescale of that process. In a way, the MSM is dividing all the dynamics in your simulations into motions that occur on different timescales.\n",
    "\n",
    "Here are a few reasons why you may find MSMs useful:\n",
    "* The free energy landscape can easily be computed from the first eigenvector\n",
    "* We can find the slowest motions, e.g. slow conformational changes and their timescales\n",
    "* We get probabilities of different transition pathways and can easily visualize networks\n",
    "* Since we only care about transitions, multiple simulations can be \"stitched together\" so we can reach timescales much longer than any individual simulation\n",
    "* We can calculate expectation values and statistical uncertainties of both stationary and dynamical molecular observables with little effort\n",
    "* Statistical analysis of MD trajectories protects us from overinterpreting \"interesting\" events which can be observed by looking at molecular movies\n",
    "* The MSMs ability to model the true dynamics can be tested, so modeling errors can be minimized"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, we will use the Python library PyEMMA, developed in the Noé group at F.U. Berlin. Have a look at the documentation, it will come in handy in this exercise: http://www.emma-project.org/latest/api/index.html.\n",
    "There are also other libraries that can be used - MSMBuilder or Enspara. GROMACS does currently not have any functionality to build MSMs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Import some useful packages\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from glob import glob\n",
    "import pyemma"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If any of the above failed, remember to activate the environment (\"source activate pyemma\" on Tegner) in which the libraries are installed *before* you start the notebook. Now, define your topology file and trajectories (they will actually be loaded into memory later)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Load your data\n",
    "top = #FIXME\n",
    "trajs = #FIXME"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to build the MSM we need to feed it a set of discrete states. But was is a state? The output from our MD simulations consist of the $(x,y,z)$ positions of each atom and how these positions evolve over time. Often, we think of the states of a molecule as the different conformations it can occupy, so we now need to transform all the $(x,y,z)$ positions into a set of features that distinguishes between these conformations in a meaningful way.\n",
    "\n",
    "Alanine dipeptide is known to be well-described by its backbone torsion angles ($\\Phi$ and $\\Psi$ angles), so these two-dimensional features have been provided for you. Now, add one high-dimensional feature consisting of all the heavy atom distances of the peptide (the function select_Heavy() might come in handy) and one other feature of your choice (low-dimensional or high-dimensional). Hint: you can look in the PyEMMA documentation for inspiration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Choose backbone torsion angles as features (2D):\n",
    "torsions_feat_def = pyemma.coordinates.featurizer(top)\n",
    "torsions_feat_def.add_backbone_torsions(periodic=False)\n",
    "torsions_data = pyemma.coordinates.load(trajs, features=torsions_feat_def)\n",
    "\n",
    "# Choose heavy atom distances as features (high-D)\n",
    "distances_data = #FIXME\n",
    "\n",
    "# Choose features of your own choice\n",
    "my_data = #FIXME"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Due to the [curse of dimensionality](https://en.wikipedia.org/wiki/Curse_of_dimensionality), we cannot directly use any of the high-dimensional features that we chose in the previous step, so we need to do some *dimensionality reduction*. There are several ways to do this, but the most commonly used within the MSM community are Principal Component Analysis (PCA) and Time-Structure Based Independent Component Analysis (tICA). PCA finds linear combinations of the degrees of freedom in the data where the *variance* is highest, while tICA finds the *slowest relaxing* degrees of freedom. In short, PCA usually results in a structural clustering, while tICA gives a kinetic clustering. Now, do both PCA and tICA on your high-dimensional distance feature set. Also, think about whether it would be appropriate to do dimensionality reduction on features you chose freely. Note that you should not do any dimensionality reduction on the torsion angles since they are already two-dimensional."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Do tICA of distances\n",
    "tica = pyemma.coordinates.tica(distances_data, lag=4, dim=2)\n",
    "tics = tica.get_output()\n",
    "\n",
    "# Do PCA of distances\n",
    "pca = pyemma.coordinates.#FIXME\n",
    "pcs = #FIXME\n",
    "\n",
    "# Do dimensionality reduction of your own features, or just set my_feats = my_data if you use low-dimensional features\n",
    "my_feats = #FIXME\n",
    "\n",
    "# Let's plot all our features \n",
    "# WARNING: never build MSMs from concatenated trajectories, this is only for plotting!\n",
    "tica_concatenated = np.concatenate(tics)\n",
    "pca_concatenated = np.concatenate(pcs)\n",
    "torsions_concatenated = np.concatenate(torsions_data)\n",
    "my_feats_concatenated = np.concatenate(my_feats)\n",
    "\n",
    "fig, axes = plt.subplots(1, 4, figsize=(14, 4))\n",
    "pyemma.plots.plot_density(*torsions_concatenated[:, :2].T, ax=axes[0], logscale=True)\n",
    "pyemma.plots.plot_density(*my_feats_concatenated[:, :2].T, ax=axes[1], logscale=True)\n",
    "pyemma.plots.plot_density(*pca_concatenated[:, :2].T, ax=axes[2], logscale=True)\n",
    "pyemma.plots.plot_density(*tica_concatenated[:, :2].T, ax=axes[3], logscale=True)\n",
    "axes[0].set_xlabel('$\\Phi$ [rad]')\n",
    "axes[0].set_ylabel('$\\Psi$ [rad]')\n",
    "axes[1].set_xlabel('MyFeature (dim1)')\n",
    "axes[1].set_ylabel('MyFeature (dim2)')\n",
    "axes[2].set_xlabel('PC 1')\n",
    "axes[2].set_ylabel('PC 2')\n",
    "axes[3].set_xlabel('tIC 1')\n",
    "axes[3].set_ylabel('tIC 2')\n",
    "axes[0].set_title('Torsion Angles')\n",
    "axes[1].set_title('My Feature')\n",
    "axes[2].set_title('PCA of Distances')\n",
    "axes[3].set_title('tICA of Distances')\n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Look at the plots above. Which one do you think best separates the different metastable (macro-)states in your simulations? \n",
    "Next, we need to discretize the features we have obtained into separate (micro-)states. This can be done with clustering algorithms like the fast K-Means, which is heavily used in many machine learning applications. The only thing we need to determine is how many (micro-)states we need. Try a few different numbers. Could there be any problem with using too many or too few states?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Discretize the data into distinct states\n",
    "nstates = #FIXME\n",
    "nstride = #FIXME\n",
    "\n",
    "torsions_clusterer = pyemma.coordinates.cluster_kmeans(torsions_data, k=nstates, stride=nstride, max_iter=30)\n",
    "tica_clusterer = pyemma.coordinates.cluster_kmeans(tics, k=nstates, stride=nstride, max_iter=30)\n",
    "pca_clusterer = pyemma.coordinates.cluster_kmeans(pcs, k=nstates, stride=nstride, max_iter=30)\n",
    "my_clusterer = pyemma.coordinates.cluster_kmeans(my_feats, k=nstates, stride=nstride, max_iter=30)\n",
    "\n",
    "# Plot your discrete states onto the feature space\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(*#FIXME.clustercenters.T, 'ko')\n",
    "pyemma.plots.plot_density(*#FIXME_concatenated[:, :2].T, ax=ax, logscale=True)\n",
    "ax.set_xlabel('#FIXME') \n",
    "ax.set_ylabel('#FIXME');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are finally ready to build some MSMs! But how can we know if the MSM is accurate? There are several ways to validate the MSM, and the two most important will be presented here (these two should always be tested before any publication is submitted!). The *Variational Principle for Conformational Dynamics* applies to MSMs made from MD data, which means that the timescales (or rather eigenvalues) are bounded from above. This means that any MSM will always yield dynamics that is too fast. By choosing a MSM that yields the slowest timescale possible ensures that we have better approximations of the true eigenvalues. In addition, we also need to ensure that the transitions in our MSM are *Markovian*, i.e. the next state is only dependent on the present state, so the MSM is \"memoryless\". This is done be introducing a *lag time*, $\\tau$ (don't confuse the MSM lag time with the tICA lag time!), which determines how sparse the frames in our simulation will be. When the MSM is Markovian, the *implied timescales*, derived from the eigenvalues, $\\lambda$, need to be constant:\n",
    "\n",
    "$\\text{ITS}(n\\tau)=-\\frac{n\\tau}{\\ln(\\lambda(n\\tau))}=-\\frac{n\\tau}{\\ln(\\lambda(\\tau))^n}=-\\frac{\\tau}{\\ln(\\lambda(\\tau))}=\\text{ITS}(\\tau)$\n",
    "\n",
    "For the final MSM, we need to choose a lag time such that the MSM is Markovian (the timescales are constant for increasing values of lag times), but still short enough such that the timescales we are interested in still are resolved, as information about the fastest processes get lost when the lag time increases.\n",
    "\n",
    "Now, generate implied timescale plots for your MSMs with different features. PyEMMA conveniently has a plotting tool implemented that also does cross-validation (shown as error bars), so we can use it directly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Choose the number of timescales to plot and upto what lag time (unit is in steps\n",
    "# so you need to keep track of the frame spacing you used when running GROMACS)\n",
    "nits = #FIXME\n",
    "lags = #FIXME\n",
    "\n",
    "# Get the implied timescales from the MSMs\n",
    "torsions_its = pyemma.msm.its(torsions_clusterer.dtrajs, lags=lags, nits=nits, errors='bayes')\n",
    "my_its = #FIXME\n",
    "pca_its = #FIXME\n",
    "tica_its = #FIXME\n",
    "\n",
    "#Plot\n",
    "fig, axes = plt.subplots(1, 4, figsize=(14, 4), sharey=True)\n",
    "pyemma.plots.plot_implied_timescales(torsions_its, ax=axes[0])\n",
    "pyemma.plots.plot_implied_timescales(my_its, ax=axes[1])\n",
    "pyemma.plots.plot_implied_timescales(pca_its, ax=axes[2])\n",
    "pyemma.plots.plot_implied_timescales(tica_its, ax=axes[3])\n",
    "axes[0].set_title(\"Torsion Angles\")\n",
    "axes[1].set_title(\"My Features\")\n",
    "axes[2].set_title(\"PCA of Distances\")\n",
    "axes[3].set_title(\"tICA of Distances\")\n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Did any of your MSMs reach convergence? Which one gives the slowest timescales? If you don't see any convergence, don't worry. It is often very difficult to reach convergence (especially for bigger systems) and building MSMs is an iterative process where you may need to either generate more data (if this is the case you usually see quite large error bars) or find better features, or combination of them. Now, choose a lag time that yields both Markovianity and resolves as many timescales as possible (the gray region shows the resolution limit)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The implied timescale test can help us determine the lag time, but it is not a very strong indicator of the validity of the model, since it only takes the eigenvalues into account while much information is still in the eigenvectors. The Chapman-Kolmogorov test is a much stronger test for validating the MSM. The CK test takes a lag time, $\\tau$, and then makes a prediction of a model quantity at lag time $k\\tau$ and this is then compared to an independently tested model at $k\\tau$. In this case, we look at the probability fo being in a set of states, $A$, after time $k\\tau$ if we started from the distribution $\\boldsymbol{w}^A$. The Chapman-Kolmogorov test then boils down to testing how well the following equation holds:\n",
    "\n",
    "$p_{MD}(A,A;k\\tau) = p_{MSM}(A,A;k\\tau)$\n",
    "\n",
    "To avoid computational overhead and a very messy visualization, PyEMMAs CK-test implementation uses a coarse-grained transition probability matrix, such that each state can be seen as a metastable state (how this is achieved will be discussed later).\n",
    "\n",
    "Now, test your best performing MSM at the lag time you chose in the previous step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Build MSM and do a CK-test\n",
    "nstates = #FIXME\n",
    "\n",
    "best_concatenated = #FIXME       #e.g. tica_concatenated\n",
    "best_clusterer = #FIXME          #e.g. tica_clusterer\n",
    "\n",
    "markov_model = pyemma.msm.bayesian_markov_model(best_clusterer.dtrajs, lag=#FIXME, dt_traj=#FIXME)\n",
    "pyemma.plots.plot_cktest(markov_model.cktest(nstates, mlags=#FIXME), dt=#FIXME, units=#FIXME);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hopefully, you now have a high-quality MSM, which means we can extract some reliable and interesting information from it! First, we will calculate the free energy landscape, which we can get from the first eigenvector. Please note that the free energy is actually only calculated for the discrete (micro-)states that we initially fed to the MSM, but pyEMMAs free energy plotting tool will extrapolate from these states such that the whole space gets a free energy estimate.\n",
    "\n",
    "Now, calculate the free energy of your best-performing MSM."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "#Plot free energy landscape\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "pyemma.plots.plot_free_energy(*best_concatenated.T, ax=ax, weights=np.concatenate(markov_model.trajectory_weights()),legacy=False)\n",
    "ax.set_xlabel('#FIXME') \n",
    "ax.set_ylabel('#FIXME')\n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we can have a look at the slowest processes in our data. Plot the six slowest processes of the best-performing MSM you used previously. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Plot the six slowest processes:\n",
    "\n",
    "eigvec = markov_model.#FIXME\n",
    "\n",
    "fig, axes = plt.subplots(2, 3, figsize=(12, 6))\n",
    "for i, ax in enumerate(axes.flat):\n",
    "    pyemma.plots.plot_contour(\n",
    "        *best_concatenated.T, eigvec[np.concatenate(best_clusterer.dtrajs), i + 1], ax=ax, cmap='jet',\n",
    "        cbar_label='{}. dynamical eigenvector'.format(i + 2), mask=True)\n",
    "    ax.scatter(*best_clusterer.clustercenters.T, s=15, c='black')\n",
    "    ax.set_xlabel('dim 1')\n",
    "    ax.set_ylabel('dim 2')\n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Are your processes going between clusters or within clusters? How many transitions between metastable states do you observe? Think about the implied timescale plot you generated previously. How many processes could you expect to be resolved by your data?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When building MSMs it is often beneficial to use hundreds, thousands or even millions of microstates (depending on your system), which makes it very difficult for a human to understand what is going on. Therefore, it is often beneficial to do yet another round of clustering, or \"lumping\", to get fewer but more physically meaningful macrostates (or metastable states). This is often done through Perron Cluster-Cluster Analysis (PCCA or PCCA+ and PCCA++ for improved versions) which estimates the clusters directly from the eigenvectors of the MSM. Each eigenvector represents a process and goes from negative to positive values. The PCCA can thus utilize the signed structure of the eigenvectors to divide the states into metastable clusters.\n",
    "\n",
    "Do PCCA lumping on your best-performing MSM. How many macrostates would be meaningful to use? The stationary probabilities for each metastable state are also computed for you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Do lumping with PCCA++ and print stationary probabilities\n",
    "nmacrostates = #FIXME\n",
    "markov_model.pcca(nmacrostates)\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(8, 6))\n",
    "_, _, plot = pyemma.plots.plot_state_map(\n",
    "    *best_concatenated.T, markov_model.metastable_assignments[np.concatenate(best_clusterer.dtrajs)], ax=ax, zorder=-1)\n",
    "plot['cbar'].set_ticklabels(range(1, nmacrostates + 1))\n",
    "ax.scatter(*best_clusterer.clustercenters.T, s=15, c='black')\n",
    "\n",
    "# Print the stationary probabilities\n",
    "for i, j in enumerate(markov_model.metastable_sets):\n",
    "    print('π_{} = {:f}'.format(i + 1, markov_model.pi[j].sum()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Look at your metastable states. Do you see any similarities between the plots for the slowest processes that you generated previously? Are the stationary probability distributions for the metastable states reasonable? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, you have completed this notebook and made simple Markov state model of alanine dipeptide! There is much more you can do to explore your MSM. You can for example look into Transition Path Theory (TPT) or other things you can find in the PyEMMA documentation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
